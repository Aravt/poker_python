'''
Module poker 
fonctions de base du jeu pour générer, évaluer et comparer des mains
'''

def chaine(chiffre:int) -> str:
    '''
    convertit un nombre en JQKA et en str.
    chaine(13)
    >>>'K'
    '''
    INT_TO_STR:dict = {11:'J' , 12:'Q' , 13:'K' , 14:'A'} #Dictionnaire contenant les corres entre les int et les str
    if chiffre > 10: #S'il faut mettre une lettre à la place du chiffre
        lettre:str = INT_TO_STR[chiffre] #On utilise le dictionnaire
    else: #Sinon
        lettre:str = str(chiffre) #On convertit simplement en chaîne de caractère
    return lettre #On retourne la lettre

def entier(lettre:str) -> int:
    '''
    convertit un nombre en str vers un entier. Permet d'obtenir 11 12 13 14 à partir de JQKA
    entier("J")
    >>>11
    '''
    STR_TO_INT:dict = {'J':11 , 'Q':12 , 'K':13 , 'A':14} #Dictionnaire str vers int pour les lettres
    if lettre.isnumeric(): #Si la lettre est un nombre
        chiffre:int = int(lettre) #On convertit en entier
    else: #Sinon -> Si c'est une lettre
        chiffre:int = STR_TO_INT[lettre] #On prend l'équivalent dans le dictionnaire
    return chiffre #On retourne le chiffre obtenu

def jeuCartes() -> list:
    '''
    Cree un jeu de 52 cartes 
    chaque  carte est une liste [[symbole:str , nombre:str] , [symbole:str , nombre:str] ...]
    Aucun paramètre
    j = jeuCartes()
    print(j[0])
    >>>["♠","2"]
    '''
    symboles:list = ["♠","♥","♣","♦"] #Une liste des 4 symboles dans un jeu de cartes
    jeu:list = [] #Pour l'instant le jeu est une liste vide
    for s in symboles: #s prend les valeurs de chaque symbole
        for c in range (2 , 15): #c prend les valeurs de 2 à 14 -> les chiffres d'un jeu
            c:str = chaine(c) #on transforme le chiffre en chaîne de caractère avec la fonction
            jeu.append([s , c]) #On ajoute dans le jeu une liste avec un symbole et un chiffre
    return jeu #On retourne le jeu 

def pioche(jeu:list , tirs:int):
    '''
    Met un nombre donne de carte d'un jeu donné dans une liste et retire les cartes du jeu
    Les cartes tirées sont aléatoires
    jeu , carte = pioche(jeuCartes() , 1)
    print(carte)
    >>>["♣" , "K"]
    '''

    from random import randint #On importe la fonction randint du module random
    carte_piochees:list = [] #Une liste qui contiendra les cartes qui seront piochées
    for t in range (tirs): #Répéter tirs fois
        numero_carte:int = randint(1,len(jeu)-1) #Donner un nombre aléatoire qui correspondra à un index de cartes
        carte:list = jeu[numero_carte] #Prendre la carte correspondante au numéro
        carte_piochees.append(carte) #Ajouter la carte aux cartes piochées
        del(jeu[numero_carte]) #Supprimer la carte du paquet
    return (jeu , carte_piochees) #Renvoyer le jeu et les cartes. Type tuple

def mainChiffre(main:list) ->list:
    '''
    renvoie uniquement les chiffres d'une main
    mainChiffre([['♥', '2'], ['♦', '9'], ['♥', 'A'], ['♥', '6'], ['♥', '7']])
    >>>[2,9,14,6,7]
    '''
    liste_chiffres:list = [] #Création d'une liste vide
    for carte in main: #Pour chaque carte
        chiffre:int = entier(carte[1]) #Obtenir le nombre correspondant avec la fonction entier()
        liste_chiffres.append(chiffre) #Ajouter le nombre à la liste
    return liste_chiffres #Retourner la liste des chiffres

def listePaires(main:list) -> list:
    '''
    renvoie le nombre d'apparition de chaque nombre de la main.
    Permet de déterminer s'il y a des paires , des brelans , des carrés...
    Conserve le même ordre pour pouvoir exploiter l'index
    listePaires([['♠', '9'], ['♣', 'Q'], ['♣', '10'], ['♥', 'Q'], ['♠', '4']])
    >>>[1,12,10,12,4]
    '''
    liste_app:list = [0,0,0,0,0] #On crée une liste du nombre d'apparitions de chaque carte (pour l'instant 0)
    l:int = len(main) #Calcul du nombre d'élement dans la main
    for k in range(l): #Répéter autant de fois qu'il y a d'éléments dans main
        carte = main[k][1] #Obtenir le nombre de la carte
        for i in range(l): #Répéter autant de fois qu'il y a d'éléments dans main
            carte_a_tester = main[i][1] #Obtenir le nombre de la carte
            if carte == carte_a_tester: #S'il y a une correspondance
                liste_app[i] += 1 #Ajouter 1 à la position correspondante de la liste
    return liste_app #Retourner la liste

def hauteur(main:list) -> int:
    '''
    Détermine la carte avec le plus de valeur dans la main.
    Ne doit pas faire partie d'une paire
    hauteur([['♠', '9'], ['♣', 'Q'], ['♣', '10'], ['♥', 'Q'], ['♠', '4']])
    >>>10
    '''
    liste_paires:list = listePaires(main) #Obtenir les récurrences des cartes avec la fonction précédente
    liste_1:list = [] #Liste vide qui contiendra les cartes ne faisant pas partie d'une paire
    for k in range(len(main)): #k devrait prendre consécutivement les valeurs 0,1,2,3,4
        if liste_paires[k] == 1: #Si l'élément k de la liste des paires est un 1
            chiffre:int = entier(main[k][1]) #Obtenir le chiffre correspondant dans la main
            liste_1.append(chiffre) #Ajouter le chiffre à la liste des hauteurs possibles
    if mainChiffre(main).sort() == [2,3,4,5,14]: #Si j'ai la suite A2345
        return 5 #La hauteur est égale à 5
    if len(liste_1) == 0: #Si aucune carte n'est isolée -> en cas de full
        return 0 #La hauteur retournée est 0
    else: #Sinon
        return max(liste_1) #On retourne le maximum de la liste des cartes isolées

def triMain(Main:list) ->list:
    '''
    Trie les mains de façon à donner les cartes plus faibles d'abord
    triMain([['♠', '9'], ['♣', 'Q'], ['♣', '10'], ['♥', 'Q'], ['♠', '4']])
    >>>[['♠', '4'], ['♠', '9'], ['♣', '10'], ['♣', 'Q'], ['♥', 'Q']]
    '''
    main:list = Main[:] #On travaille sur une copie de la main car on va la modifier
    chiffres:list = mainChiffre(main) #Obtenir chaque chiffre de la main avec la fonction
    chiffres.sort() #Trier cette liste de chiffre
    main_triee:list = [] #Liste vide qui contiendra la main triée
    for c in chiffres: #c prendra consécutivement les valeurs de chaque chiffre
        for carte in main: #Pour chaque carte de la main
            index:int = main.index(carte) #Obtenir la position de la carte dans la liste
            if entier(carte[1]) == c: #Si le chiffre de la carte est égal au chiffre de la liste des nombres
                main_triee.append(carte) #Ajouter la carte à la liste triée 
                del main[index] #Supprimer la carte dans la copie de la main
    return main_triee #Retourner la main triée

def estFlush(main:list) -> bool:
    """
    Renvoie True si on a une couleur -> si tous les symboles de la main sont identiques
    estFlush([['♠', '9'], ['♣', 'Q'], ['♣', '10'], ['♥', 'Q'], ['♠', '4']])
    >>>False
    """
    flush:bool = True #Par défaut on a une couleur
    compteur:int = 0 #Compteur qui servira à tester les différentes cartes
    while flush and (compteur < len(main) - 1): #Répéter tant que j'ai une couleur et que je n'ai pas fait le tour de la main
            flush = main[compteur][0] == main[compteur + 1][0] #flush est booleen. Il vérifie si le symbole d'une carte est égal au suivant
            compteur += 1 #On incrémente
    return flush #On retourne flush

def estQuinte(main:list) -> bool:
    """
    Retourne True si la main est une suite
    estQuinte([['♠', '9'], ['♣', '10'], ['♣', 'J'], ['♥', 'Q'], ['♠', 'K']])
    >>>True
    """
    main_triee = triMain(main) #On trie la main 
    compteur:int = 0 #Compteur pour le while
    quinte:bool = True #Par défaut on a une suite
    while quinte and (compteur < len(main) - 1): #Tant que j'ai une suite et que je n'ai pas parcouru tous les éléments
            quinte = (entier(main_triee[compteur][1]) + 1) == (entier(main_triee[compteur + 1][1])) #Quinte est True si l'élément désigné + 1 est égal au suivant -> augmentation de 1
            compteur += 1 #Incrémentation
    return quinte or (mainChiffre(main_triee) == [2,3,4,5,14]) #Quinte est aussi vrai si on a A2345

def estQuinteFlush(main:list) -> bool:
    """
    On a une quinte flush avec une suite et une couleur
    estQuinteFlush([['♠', '9'], ['♣', 'Q'], ['♣', '10'], ['♥', 'Q'], ['♠', '4']])
    >>>False
    """
    return estQuinte(main) and estFlush(main) #True si les deux fonctions estQuinte et estFlush renvoient True

def estQuinteFlushRoyale(main:list) -> bool:
    """
    Une quinte flush royale est une quinte flush avec une hauteur As
    estQuinteFlushRoyale([['♠', '9'], ['♠', 'K'], ['♠', '10'], ['♠', 'A'], ['♠', 'J']])
    >>>True
    """
    return estQuinteFlush(main) and (hauteur(main) == 14) #True si la fonction estQuinteFlush renvoie True et la fonction hauteur renvoie 14 (As)

def estCarre(main:list) ->int:
    '''
    Dit si on a 4 cartes de même numéro. Si oui nous renvoie le numéro en question , sinon 0
    estCarre([['♠', '9'], ["♦", '9'], ["♥", '9'], ['♠', 'A'], ["♣", '9']])
    >>>9
    '''
    liste_paires:list = listePaires(main) #Obtenir la liste des apparitions des paires dans la main
    est_carre:bool =  4 in liste_paires #Si une carte apparait 4 fois dans la main -> carré
    if est_carre: #Si on a un carré
        carre:int = entier(main[liste_paires.index(4)][1]) #Déterminer à quelle position il est pour avoir le num
    else: #Sinon -> pas de carré
        carre:int = 0 #Carré vaut 0
    return carre #Retourner carré

def estBrelan(main:list) ->int:
    '''
    Determine si la main a 3 cartes du meme type
    retourne le nombre du brelan , 0 sinon
    Un carré n'est pas un brelan
    estBrelan([['♠', '9'], ["♦", '9'], ["♥", '9'], ['♠', 'A'], ["♣", '9']])
    >>>0
    '''
    liste_paires:int = listePaires(main) #Obtenir la liste des correspondances 
    est_brelan:bool = 3 in liste_paires #Savoir si une carte revient 3 fois
    if est_brelan: #Si on a un brelan
        brelan:int = entier(main[liste_paires.index(3)][1]) #Obtenir son nombre avec l'index 
    else: #Sinon 
        brelan:int = 0 #Brelan vaut 0
    return brelan #Retourner la valeur du brelan

def estPaire(main:list) -> list:
    """
    Determine si on a des paires 
    les met dans une liste et les trie de façon à donner les plus grandes valeurs d'abord
    si on a pas de paire la liste est vide
    si on en a une elle contient 1 element
    si on en a 2 deux elements
    les carres et les brelans ne sont pas des paires
    estPaire([['♠', '9'], ["♦", '9'], ["♥", 'A'], ['♠', 'A'], ["♣", '7']])
    >>>[14,9]
    """
    main_chiffre :list= mainChiffre(main) #obtenir la liste des chiffres de la main
    l:list = len(main) #Obtenir le nombre d'elements dans la main
    liste_paires:list = listePaires(main) #La liste des apparitions
    les_paires:list = [] #Liste qui contiendra nombres des paires
    for k in range(l): #Repeter autant de fois qu'il y a d'elements dans la main
        if liste_paires[k] == 2: #Si l'élement qu'on inspecte dans la liste des paires est 2
            numero:int = main_chiffre[k] #On obtient sa valeur dans les chiffres de la main
            if numero not in les_paires: #Si le numéro ne fait pas déja partie de la liste finale
                les_paires.append(numero) #L'y ajouter
    les_paires.sort() #Trier la liste
    les_paires.reverse() #Inverser son ordre
    return les_paires #Retourner la liste

def estFull(main:list) -> bool:
    """
    Un full est une paire + un brelan
    Renvoie True si c'est la combinaison de la main
    estFull([['♠', '9'], ["♦", '9'], ["♥", '9'], ['♠', 'A'], ["♣", '9']])
    >>>False
    """
    liste_paires:list = listePaires(main) #Obtenir les correspondances
    return (2 in liste_paires) and (3 in liste_paires) #True s'il y a des 3 et des 2 dans liste_paires

def evalMain(main:list):
    main2 = list(main)
    h = hauteur(main2)
    liste_combi = [0,0,0]
    liste_combi.append(h)
    carre = estCarre(main2)
    brelan = estBrelan(main2)
    paires = estPaire(main2)
    if estQuinteFlushRoyale(main2):
        liste_combi[0] = 9
    elif estQuinteFlush(main2):
        liste_combi[0] = 8
    elif carre > 0:
        liste_combi[0] = 7
        liste_combi[1] = carre
    elif estFull(main2):
        liste_combi[0] = 6
        liste_combi[1] = estBrelan(main2)
        liste_combi[2] = estPaire(main2)[0]
    elif estFlush(main2):
        liste_combi[0] = 5
    elif estQuinte(main2):
        liste_combi[0] = 4
    elif brelan > 0:
        liste_combi[0] = 3
        liste_combi[1] = brelan
    elif len(paires) == 2:
        liste_combi[0] = 2
        liste_combi[1] = paires[0]
        liste_combi[2] = paires[1]
    elif len(paires) == 1:
        liste_combi[0] = 1
        liste_combi[1] = paires[0]
    return liste_combi

def comparMain(main1 , main2):
    score_1 = evalMain(list(main1))
    score_2 = evalMain(list(main2))
    compteur_main = 0
    gagnant = 0
    while gagnant == 0 and compteur_main < 4:
        if score_1[compteur_main] > score_2[compteur_main]:
            gagnant = 1
        elif score_1[compteur_main] < score_2[compteur_main]:
            gagnant = 2
        compteur_main += 1
    return gagnant

def msgMain(main):
    combi = evalMain(main)
    for k in range (1,4):
        combi[k] = chaine(combi[k])
    if combi[0] == 9:
        msg = 'quinte flush royale'
    elif combi[0] == 8:
        msg = 'quinte flush , hauteur {}'.format(combi[3])
    elif combi[0] == 7:
        msg = 'carré de {} , kicker {}'.format(combi[1],combi[3])
    elif combi[0] == 6:
        msg = 'full aux {} par les {}'.format(combi[1] , combi[2])
    elif combi[0] == 5:
        msg = 'couleur , hauteur {}'.format(combi[3])
    elif combi[0] == 4:
        msg = 'suite , hauteur {}'.format(combi[3])
    elif combi[3] == 4:
        msg = 'brelan de {} , kicker {}'.format(combi[1] , combi[3])
    elif combi[0] == 2:
        msg = 'paires de {} et de {} , kicker {}'.format(combi[1] , combi[2] , combi[3])
    elif combi[0] == 1:
        msg = 'paire de {} , kicker {}'.format(combi[1] , combi[3])
    else:
        msg = 'hauteur {}'.format(combi[3])
    return msg

def chemin(carte:list) -> str:
    '''
    renvoie le chemin relatif de l' carte
    '''
    return 'cartes/{}/{}.png'.format(carte[0] , carte[1])