from module_poker import jeuCartes , pioche , comparMain ,msgMain
from itertools import combinations

class Joueur:

    nb_joueurs:int = 0
    nb_joueurs_temp:int = 0
    nb_joueurs_restants:int = 0
    liste_joueurs:list = []
    liste_joueurs_temp:list = []
    liste_joueurs_restants:list = []
    liste_sous:list = []
    pot:int = 0
    jeu:list = jeuCartes()
    joueur_actuel:int = 0
    mise_max:int = 0
    aligne:bool = False
    milieu:list = []
    tour:int = 0

    def __init__(self,nom:str):
        self.nom:str = nom
        self.argent:str = 1500
        self.mise:int = 0
        self.cartes:list = []
        self.a_joue:bool = False
        self.est_tapis:bool = False
        self.est_dealer:bool = False
        self.est_petite_blinde:bool = False
        self.est_grosse_blinde:bool = False
        self.est_utg:bool = False
        self.combi:list = []
        Joueur.nb_joueurs += 1
        Joueur.liste_joueurs.append(self)

    def __repr__(self):
        if self.mise > 0:
            return '{} ({}) : {}'.format(self.nom.center(20 , ' ') , self.argent , str(self.mise).center(20,' '))
        else:
            return '{} ({}) :'.format(self.nom.center(20,' ') , self.argent)

    def affJoueurs(cls):
        for j in cls.liste_joueurs:
            print(j)
    affJoueurs = classmethod(affJoueurs)

    def affJoueursRestants(cls):
        for j in cls.liste_joueurs_restants:
            print(j)
    affJoueursRestants = classmethod(affJoueursRestants)

    def initJeu(cls):
        cb_joueurs:str = input('Combien de joueurs ? : ')
        while not (cb_joueurs.isnumeric or cb_joueurs == ''):
            print('Nombre invalide !')
            cb_joueurs:str = input('Combien de joueurs ? : ')
        cb_joueurs:int = int(cb_joueurs)
        for j in range(cb_joueurs):
            nom:str = input('Nom du joueur {} : '.format(j + 1))
            Joueur(nom)
        del nom
        del cb_joueurs
        cls.liste_sous = [1500] * cls.nb_joueurs
        cls.liste_joueurs[0].est_dealer = True
        cls.liste_joueurs[1].est_petite_blinde = True
        if cls.nb_joueurs == 2:
            cls.liste_joueurs[0].est_grosse_blinde = True
        else:
            cls.liste_joueurs[2].est_grosse_blinde = False
    initJeu = classmethod(initJeu)

    def initPot(cls):
        cls.nb_joueurs_restants = cls.nb_joueurs
        cls.liste_joueurs_restants = cls.liste_joueurs
        cls.jeu = jeuCartes()
        cls.pot = 0
        cls.liste_sous = []
        cls.joueur_actuel = 0
        cls.aligne = False
        cls.mise_max = 0
        cls.milieu =[]
        cls.tour = 0
        cls.nb_joueurs_temp = cls.nb_joueurs
        cls.liste_joueurs_temp = cls.liste_joueurs
        for j in cls.liste_joueurs:
            cls.liste_sous.append(j.argent)
            j.mise = 0
            j.cartes = []
            j.a_joue = False
            j.est_tapis = False
            if j.est_dealer:
                j.est_dealer = False
                i = cls.liste_joueurs.index(j)
                try:
                    cls.liste_joueurs[i + 1].est_dealer = True
                except:
                    cls.liste_joueurs[0].est_dealer = True
            if j.est_petite_blinde:
                j.est_petite_blinde = False
                i = cls.liste_joueurs.index(j)
                try:
                    cls.liste_joueurs[i + 1].est_petite_blinde = True
                except:
                    cls.liste_joueurs[0].est_petite_blinde = True
            if j.est_grosse_blinde:
                j.est_grosse_blinde = False
                i = cls.liste_joueurs.index(j)
                try:
                    cls.liste_joueurs[i + 1].est_grosse_blinde = True
                except:
                    cls.liste_joueurs[0].est_grosse_blinde = True
    initPot = classmethod(initPot)

    def distrib(cls):
        print('Distribution des cartes')
        for j in cls.liste_joueurs:
            cls.jeu , cartes = pioche(cls.jeu , 2)
            j.cartes = cartes
    distrib = classmethod(distrib)

    def fold(self):
        print('{} se couche'.format(self.nom))
        Joueur.liste_joueurs_restants.remove(self)
        Joueur.nb_joueurs_restants -= 1
        self.a_joue = True

    def check(self):
        print('{} check'.format(self.nom))
        self.a_joue = True

    def bet(self , mise:int):
        if mise >= self.argent:
            print('{} mise {} et est à tapis !')
            self.mise += self.argent
            Joueur.pot += self.argent
            self.argent = 0 
            self.est_tapis = True
            Joueur.mise_max = max(Joueur.mise_max , self.mise)
        else:
            self.mise += mise
            Joueur.pot += mise
            self.argent -= mise
            if self.mise == Joueur.mise_max:
                print('{} suit {}'.format(self.nom , mise))
            else:
                print('{} relance a {}'.format(self.nom , mise))
                Joueur.mise_max = self.mise
        self.a_joue = True

    def miser(self):
        a_suivre:int = Joueur.mise_max - self.mise
        mise:str = input('Combien misez-vous ? ({} pour suivre)'.format(a_suivre))
        if not (mise.isnumeric or mise == ''):
            mise:int = 0
        else:
            mise:int = abs(int(mise))
        if mise == 0:
            if Joueur.mise_max > 0:
                self.fold()
            else:
                self.check()
        else:
            self.bet(mise)

    def tousAlignes(cls):
        tous_joue = True
        j = 0
        while tous_joue and j < cls.nb_joueurs_restants:
            tous_joue = cls.liste_joueurs[j].a_joue
            j += 1
        cls.aligne = tous_joue
        j = 0
        while cls.aligne and j < cls.nb_joueurs_restants:
            cls.aligne = cls.liste_joueurs_restants[j].mise == cls.mise_max
            j += 1
    tousAlignes = classmethod(tousAlignes)

    def sousous(cls):
        cls.liste_sous = [j.argent for j in cls.liste_joueurs]
        for j in cls.joueur:
            if j.argent == 0:
                cls.liste_joueurs.remove(j)
                cls.nb_joueurs -= 1
    sousous = classmethod(sousous)

    def placerBlindes(cls):
        for j in cls.liste_joueurs:
            if j.est_petite_blinde:
                print('{} place la petite blinde'.format(j.nom))
                if j.argent<= 10:
                    j.est_tapis = True
                    cls.pot += j.argent
                    j.argent = 0
                else:
                    j.argent -= 10
                    cls.pot += 10
            if j.est_grosse_blinde:
                print('{} place la grosse blinde'.format(j.nom))
                if j.argent<= 20:
                    j.est_tapis = True
                    cls.pot += j.argent
                    j.argent = 0
                else:
                    j.argent -= 20
                    cls.pot += 20
    placerBlindes = classmethod(placerBlindes)

    def ajouterCartes(cls,nb_cartes=1):
        cls.jeu , cls.milieu = pioche(cls.jeu , nb_cartes)
    ajouterCartes = classmethod(ajouterCartes)

    def combiMain(self):
        from itertools import combinations
        cartes_dispo = Joueur.milieu + self.cartes
        combis = list(combinations(cartes_dispo , 5))
        maxi = combis[0]
        for c in combis:
            if comparMain(c , maxi) == 1:
                maxi = c
        self.combi = c

Joueur.initJeu()
while nb_joueurs > 1:
    Joueur.initPot()
    Joueur.distrib()
    Joueur.placerBlindes()
    Joueur.affJoueurs()
    while (not Joueur.aligne) and nb_joueurs > 1:
        Joueur.joueur_actuel += 1
        Joueur.liste_joueurs_restants[Joueur.joueur_actuel]